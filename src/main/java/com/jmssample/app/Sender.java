package com.jmssample.app;

import javax.jms.Queue;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSDestinationDefinition;

@Stateless
@JMSDestinationDefinition(name = "java:global/jms/queue",
        interfaceName = "javax.jms.Queue")
public class Sender {

    @Resource
    private ConnectionFactory factory;

    @Resource(mappedName = "java:global/jms/queue")
    Queue queue;

    public void sendMessage(String message) {
        try (JMSContext context = factory.createContext()) {
            context.createProducer().send(queue, message);
        }
    }
}