package com.jmssample.app;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Stateless
public class Receiver {

    @Resource
    private ConnectionFactory factory;

    @Resource(mappedName="java:global/jms/queue")
    Queue myQueue;

    public String receiveMessage() {
        try (JMSContext context = factory.createContext()) {
            return context.createConsumer(myQueue).receiveBody(String.class, 1000);
        }
    }
}