/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jmssample.app;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JMSSample extends HttpServlet {

    @Inject
    Sender sender;

    @Inject
    Receiver receiver;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        String message = request.getParameter("message");

        String result = message;
        sender.sendMessage(message);


        if (result != null) {
            request.setAttribute("result", receiver.receiveMessage());
            RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("index.jsp");
        }
    }

}